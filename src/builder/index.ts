/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

import {
    Forms,
    NodeBlock,
    REGEX_IS_URL,
    definition,
    editor,
    insertVariable,
    isString,
    pgettext,
    tripetto,
} from "tripetto";
import { IStatement } from "../runner";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    alias: "statement",
    get label() {
        return pgettext("block:statement", "Statement");
    },
})
export class Statement extends NodeBlock implements IStatement {
    @definition("string", "optional")
    imageURL?: string;

    @definition("string", "optional")
    imageWidth?: string;

    @definition("boolean", "optional")
    imageAboveText?: boolean;

    @editor
    defineEditor(): void {
        this.editor.name(false, true, pgettext("block:statement", "Statement"));
        this.editor.description();
        this.editor.option({
            name: pgettext("block:statement", "Image"),
            form: {
                title: pgettext("block:statement", "Image"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "imageURL", undefined)
                    )
                        .label(pgettext("block:statement", "Image source URL"))
                        .inputMode("url")
                        .placeholder("https://")
                        .action("@", insertVariable(this))
                        .autoValidate((ref: Forms.Text) =>
                            ref.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(ref.value) ||
                                  (ref.value.length > 23 &&
                                      ref.value.indexOf(
                                          "data:image/jpeg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/png;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/svg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/gif;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 1 &&
                                      ref.value.charAt(0) === "/")
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Text(
                        "singleline",
                        Forms.Checkbox.bind(this, "imageWidth", undefined)
                    )
                        .label(
                            pgettext(
                                "block:statement",
                                "Image width (optional)"
                            )
                        )
                        .width(100)
                        .align("center"),
                    new Forms.Checkbox(
                        pgettext(
                            "block:statement",
                            "Display image on top of the paragraph"
                        ),
                        Forms.Checkbox.bind(this, "imageAboveText", undefined)
                    ),
                ],
            },
            activated: isString(this.imageURL),
        });
        this.editor.explanation();

        this.editor.groups.options();
        this.editor.visibility();
    }
}
